
# Introduction

## Installation

This guide is tested on ubuntu 18.04 LTS

```bash
apt update
apt install memcached libmemcached-tools pwgen libsasl2-dev -y
test -d /etc/i-tee || mkdir -p /etc/i-tee

#disable UDP
echo '-U 0' >> /etc/memcached.conf
# enable SASL
echo '-S' >> /etc/memcached.conf

systemctl restart memcached

apt install sasl2-bin

mkdir -p /etc/sasl2

cat > /etc/sasl2/memcached.conf <<END
mech_list: plain
log_level: 5
sasldb_path: /etc/sasl2/memcached-sasldb2
END

PASS=$(pwgen 20 1)
echo "memcached SASL username:mc password: ${PASS}" >> /root/i-tee-passwords.txt
echo $PASS | saslpasswd2 -a memcached -c -p -f /etc/sasl2/memcached-sasldb2 mc
chown memcache:memcache /etc/sasl2/memcached-sasldb2

echo "MEMCACHE_USERNAME=mc" >> /etc/i-tee/memcache.env
echo "MEMCACHE_PASSWORD=${PASS}" >> /etc/i-tee/memcache.env

VBOXMANAGER_TOKEN=$(pwgen 20 1)
echo "VBOXMANAGER_TOKEN=${VBOXMANAGER_TOKEN}" >> /etc/i-tee/memcache.env

chmod go= /etc/i-tee/memcache.env

```

### Memcache setup

install gems with `bundle install`

generate a token and save it to `app/config.json` or `ÈNV['VBOXMANAGER_TOKEN']` This will be the API key to be used in requests documented below.

## Public API

```
curl -H "Content-Type: application/json" -X GET http://localhost:4567/vms/running
```

```
curl -H "Content-Type: application/json" -X GET http://localhost:4567/vms/stopped
```

```
curl -H "Content-Type: application/json" -X GET http://localhost:4567/vms/all
```

```
curl -H "Content-Type: application/json" -X GET http://localhost:4567/vms/templates
```

```
curl -H "Content-Type: application/json" -X GET http://localhost:4567/vms/search/:searchword
```

```
curl -H "Content-Type: application/json" -X GET http://localhost:4567/vms/ips/:search
```

```
curl -H "Content-Type: application/json" -X GET http://localhost:4567/vms/ips/
```

## Private API

* all these endpoints have an compulsory data field called 'apiKey' used for authorization
* all these endpoints have an optional boolean data field called 'sync', which determines if the vbox commands should be run synchronously or not.

```
curl -H "Content-Type: application/json" -X GET http://localhost:4567/vms/info.json -d '{"name":"vmName", "apiKey":"apitoken"}'
```

```
curl -H "Content-Type: application/json" -X POST http://localhost:4567/vms/clone.json -d '{"template":"existingVmName", "name":"newVmName", "snapshot":"snapshotName", "apiKey":"apitoken"}'
```

```
curl -H "Content-Type: application/json" -X POST http://localhost:4567/vms/snapshot.json -d '{"name":"vmName", "apiKey":"apitoken"}'
```

```
curl -H "Content-Type: application/json" -X PUT http://localhost:4567/vms/start.json -d '{"name":"vmName", "apiKey":"apitoken"}'
```

```
curl -H "Content-Type: application/json" -X PUT http://localhost:4567/vms/stop.json -d '{"name":"vmName", "apiKey":"apitoken"}'
```

```
curl -H "Content-Type: application/json" -X PUT http://localhost:4567/vms/pause.json -d '{"name":"vmName", "apiKey":"apitoken"}'
```

```
curl -H "Content-Type: application/json" -X PUT http://localhost:4567/vms/resume.json -d '{"name":"vmName", "apiKey":"apitoken"}'
```


```
curl -H "Content-Type: application/json" -X PUT http://localhost:4567/vms/port.json -d '{"name":"vmName", "range":"9000-11000","apiKey":"apitoken"}'
```

```
curl -H "Content-Type: application/json" -X PUT http://localhost:4567/vms/extradata.json -d '{"name":"vmName", "key":"fieldName", "value":"fieldValue","apiKey":"apitoken"}'
```

```
curl -H "Content-Type: application/json" -X PUT http://localhost:4567/vms/extradata.json -d '{"name":"vmName", "values":[{"key":"fieldName", "value":"fieldValue"}],"apiKey":"apitoken"}'
```

```
curl -H "Content-Type: application/json" -X PUT http://localhost:4567/vms/groups.json -d '{"name":"vmName", "groups":["groupName"],"apiKey":"apitoken"}'
```

```
curl -H "Content-Type: application/json" -X PUT http://localhost:4567/vms/reset_rdp.json -d '{"name":"vmName", "apiKey":"apitoken"}'
```


```
curl -H "Content-Type: application/json" -X PUT http://localhost:4567/vms/set_network.json -d '{"name":"vmName", "slot":0, "type":"networkType", "nw_name":"networkName", "apiKey":"apitoken"}'
```

```
curl -H "Content-Type: application/json" -X PUT http://localhost:4567/vms/set_running_network.json -d '{"name":"vmName", "slot":0, "type":"networkType", "nw_name":"networkName", "apiKey":"apitoken"}'
```


```
curl -H "Content-Type: application/json" -X DELETE http://localhost:4567/vms/delete.json -d '{"name":"vmName", "apiKey":"apitoken"}'
```

## LICENSE

This project is published with the MIT License 