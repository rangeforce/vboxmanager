require 'fileutils'
class VirtualBox
	@@prefix = "utils/vboxmanage" # use the script

	@@vm_mutex = Mutex.new

	def self.vboxmanage(cmnd, sync=true) # if vm_mutex is not false use syncronize
		stdout = ''
		if @@vm_mutex && sync
			@@vm_mutex.synchronize do
				stdout = %x(#{@@prefix} #{cmnd} )
			end
		else # either mutex not set or set to not sync
			stdout = %x(#{@@prefix} #{cmnd})
		end
		status = $?.exitstatus
		return {stdout: stdout, exitstatus: status }
	end

	def self.resources
		stdout = ''
		stdout = %x(./utils/check-resources)
		if $?.exitstatus === 0
			{status: 200, data: "Sufficient resources found"}
		else
			{status: 503, data: 'Sorry, there are currently not enough resources to start the attempt. Please try again in a while.'}
		end
	end

	# get application version info. git commit, ruby version
	def self.versions
		info = {status: 200, data:"VboxManager versions", versions: {git: "N/A", date:"N/A", ruby: "N/A"}}
		stdout = ''
		stdout = %x(git log -1 --pretty=format:"%h | %ad" )
		if $?.exitstatus === 0
			i = stdout.split(' | ')
			info[:versions][:git] = i.first.strip
			info[:versions][:date] = i.last.strip
		else
			info[:status] = 500
			info[:data] = 'Unable to get git info!'
		  return info
		end
		stdout = %x(ruby -v )
		if $?.exitstatus === 0
			i = stdout.split(' ')
			info[:versions][:ruby] = "ruby-"+i[1].strip
		else
			info[:status] = 500
			info[:data] = 'Unable to get ruby info!'
		  return info
		end
		info
	end


	def self.running_vms(try_again=false)
		MyLog.log.info "RUNNINGVMS CALLED: try_again=#{try_again}"
		retry_sleep = 1
		if try_again
			(0..5).each do |try|
				result = VirtualBox.vboxmanage("list runningvms 2>&1", false)
				unless result[:exitstatus] == 0 #check if the child process exited cleanly.
					if try<5
						MyLog.log.warn "RUNNINGVMS: failed try=#{try}/5\n #{result[:stdout]}"
						sleep retry_sleep
						next
					else # last try
						MyLog.log.error "RUNNINGVMS FAILED try=#{try}/5 \n#{result[:stdout]}"
						return {status: 500, data: "Unable to read running vms" }
					end
				end 
				MyLog.log.info "RUNNINGVMS SUCCESS: try=#{try}/5"
				return {status: 200, data: result[:stdout]}
			end
		else # no retry
			result = VirtualBox.vboxmanage("list runningvms 2>&1", false)
			unless result[:exitstatus] == 0 #check if the child process exited cleanly.
				MyLog.log.error "RUNNINGVMS FAILED \n#{result[:stdout]}"
				return {status: 500, data: "Unable to read running vms" }
			end
			MyLog.log.info "RUNNINGVMS SUCCESS"
			return {status: 200, data: result[:stdout]}
		end
	end

	def self.vms(try_again=false)
		MyLog.log.info "VMS CALLED: try_again=#{try_again}"
		retry_sleep = 1
		if try_again
			(0..5).each do |try|
				result = VirtualBox.vboxmanage("list vms 2>&1", false)
				unless result[:exitstatus] == 0 #check if the child process exited cleanly.
					if try<5
						MyLog.log.warn "VMS: failed try=#{try}/5\n #{result[:stdout]}"
						sleep retry_sleep
						next
					else # last try
						MyLog.log.error "VMS FAILED try=#{try}/5 \n#{result[:stdout]}"
						return {status: 500, data: "Unable to read vms" }
					end
				end 
				MyLog.log.info "VMS SUCCESS: try=#{try}/5"
				return {status: 200, data: result[:stdout]}
			end
		else # no retry
			result = VirtualBox.vboxmanage("list vms 2>&1", false)
			unless result[:exitstatus] == 0 #check if the child process exited cleanly.
				MyLog.log.error "VMS FAILED \n#{result[:stdout]}"
				return {status: 500, data: "Unable to read vms" }
			end
			MyLog.log.info "VMS SUCCESS"
			return {status: 200, data: result[:stdout]}
		end
	end

	# TODO: fails without message, replaced by vms+select in route
	def self.templates(try_again=false)
		MyLog.log.info "TEMPLATES CALLED: try_again=#{try_again}"
		retry_sleep = 1
		if try_again
			(0..5).each do |try|
				result = VirtualBox.vboxmanage("list vms | grep template 2>&1", false)
				unless result[:exitstatus] == 0 #check if the child process exited cleanly.
					if try<5
						MyLog.log.warn "TEMPLATES: failed try=#{try}/5\n #{result[:stdout]}"
						sleep retry_sleep
						next
					else # last try
						MyLog.log.error "TEMPLATES FAILED try=#{try}/5 \n#{result[:stdout]}"
						return {status: 500, data: "Unable to read template vms" }
					end
				end 
				MyLog.log.info "TEMPLATES SUCCESS: try=#{try}/5"
				return {status: 200, data: result[:stdout]}
			end
		else # no retry
			result = VirtualBox.vboxmanage("list vms | grep template 2>&1", false)
			unless result[:exitstatus] == 0 #check if the child process exited cleanly.
				MyLog.log.error "TEMPLATES FAILED \n#{result[:stdout]}"
				return {status: 500, data: "Unable to read template vms" }
			end
			MyLog.log.info "TEMPLATES SUCCESS"
			return {status: 200, data: result[:stdout]}
		end
	end



	def self.vm_info(name, try_again=true, sync=false)
		MyLog.log.debug "SHOWVMINFO CALLED: vm=#{name} "
		retry_sleep = 1 # seconds to wait before retry
		result = false
		if try_again
			(0..5).each do |try|
				MyLog.log.debug "SHOWVMINFO: try=#{try}/5 vm=#{name}"
				result = VirtualBox.vboxmanage("showvminfo #{esc(name)} --machinereadable 2>&1", sync)
				if result[:exitstatus] != 0
					MyLog.log.debug "SHOWVMINFO: failed try=#{try}/5 vm=#{name}\n#{result[:stdout]}"
					if try < 5
						sleep retry_sleep
						next  # go to next loop if not last
					else # last attempt failed, error depends on output
						if result[:stdout].start_with? "VBoxManage: error: Could not find a registered machine named '#{name}'"
							MyLog.log.warn "SHOWVMINFO: no such machine try=#{try}/5 vm=#{name}"
							return {status: 404, data: "VM '#{name}' Not found"}
						else
							MyLog.log.error "SHOWVMINFO: try=#{try}/5 vm=#{name}"
							return {status: 500, data: "Failed to get vm info for #{name}"}
						end
					end
				else
					MyLog.log.debug "SHOWVMINFO SUCCESS: try=#{try}/5 vm=#{name}"
					break # exit loop
				end
			end # eof loop
		else
			# for views for faster loading!
			result = VirtualBox.vboxmanage("showvminfo #{esc(name)} --machinereadable 2>&1", sync)
			if result[:exitstatus] != 0
				MyLog.log.debug "SHOWVMINFO: failed vm=#{name}\n#{result[:stdout]}"
				if result[:stdout].start_with? "VBoxManage: error: Could not find a registered machine named '#{name}'"
					MyLog.log.warn "SHOWVMINFO FAILED: no such machine vm=#{name}"
					return {status: 404, data: "VM '#{name}' Not found"}
				else
					MyLog.log.error "SHOWVMINFO FAILED: vm=#{name}"
					return {status: 500, data: "Failed to get vm info for #{name}"}
				end
			else
				MyLog.log.debug "SHOWVMINFO SUCCESS: vm=#{name}"
			end
		end
		vm = {}
		if result 
			result[:stdout].split(/\n+/).each do |row|
				if row.strip!=''
					f = row.split('=')
					value = f.last.gsub('"', '').gsub('<not set>', '')
					field = f.first.gsub('"', '')
						

					if field.include? '['
						subfield = field.slice(field.index('[')..field.index(']'))
						field.gsub!(subfield,'')
						subfield.gsub!('[','').gsub!( ']','')

						if subfield.include? '/'
							s=subfield.split('/')
							subsub = s.last
							subfield = s.first
						end
					end

					if subfield
						unless vm[field] # create empty hash to house the subfield
							vm[field] = {}
						end
						if subsub
							unless vm[field][subfield] # create empty hash to house the sub-subfield
								vm[field][subfield] = {}
							end
							vm[field][subfield][subsub] = value
						else
							vm[field][subfield] = value
						end
					else
						vm[field] = value
					end
				end
			end
			# field-specific parsing
			
			vm['groups'] = vm['groups'].split(',') unless vm['groups'].blank?
			if vm['CurrentSnapshotNode']
				value = vm[vm['CurrentSnapshotNode'].gsub('Name', 'Description')]
				begin
					value.to_time
				rescue
					value = ''
				end
				vm['CurrentSnapshotDescription']=value
			end
		end # eof if result
		{ status: 200, data: vm}
	end

	# TODO: think if checking if locked should not terminate looping..as in this might be a temporary state
	def self.modify_vrdeport(vm, range='9000-11000', sync=false)
		MyLog.log.info "SET PORT RANGE CALLED: vm=#{vm} range=#{range}"
		retry_sleep = 1 # seconds to wait before retry
		(0..5).each do |try|
			MyLog.log.debug "SET PORT RANGE: try=#{try}/5 vm=#{vm} range=#{range}"
			result = VirtualBox.vboxmanage("modifyvm #{esc(vm)} --vrdeport #{esc(range)}  2>&1", sync)
			if result[:exitstatus] != 0
				if result[:stdout].start_with? "VBoxManage: error: The machine '#{vm}' is already locked by a session (or being locked or unlocked)"
					# machine is running
					MyLog.log.info "SET PORT RANGE: can not set port range for running vm try=#{try}/5 vm=#{vm}"
					return {status: 200, data: "Unable to set running VM '#{vm}' port range to #{range}"} # exit with true although port range was not changed?
				else
					MyLog.log.warn "SET PORT RANGE: failed try=#{try}/5 vm=#{vm} range=#{range} \n#{result[:stdout]}"
					if try < 5
						sleep retry_sleep
						next  # go to next loop if not last
					else # last attempt failed
						MyLog.log.info "SET PORT RANGE FAILED: try=#{try}/5 vm=#{vm} range=#{range}"
						return {status: 500, data: "Failed to set VM '#{vm}' port range to #{range}"} 
					end
				end
			else # success 
				MyLog.log.info "SET PORT RANGE SUCCESS: try=#{try}/5 vm=#{vm} range=#{range}"
				return {status: 200, data: "VM '#{vm}' port range set to #{range}"}
			end
		end # end loop
	end

	def self.start_vm(vm, sync=true)
		MyLog.log.info "VM START CALLED: vm=#{vm}"
		retry_sleep = 2 # seconds to wait before retry
		(0..5).each do |try|
			MyLog.log.debug "VM START: try=#{try}/5 vm=#{vm}"
			result = VirtualBox.vboxmanage("startvm #{esc(vm)} --type headless  2>&1", sync)
			if result[:exitstatus] != 0
				if result[:stdout].start_with? "VBoxManage: error: The machine '#{vm}' is already locked by a session (or being locked or unlocked)"
					MyLog.log.info "VM START SUCCESS: already started try=#{try}/5 vm=#{vm}"
					return {status: 200, data: "VM '#{vm}' already started" } # exit if successful
				else
					MyLog.log.warn "VM START: failed try=#{try}/5 vm=#{vm} \n#{result[:stdout]}"
					if try < 5
						sleep retry_sleep
						next  # go to next loop if not last
					else # last attempt failed
						MyLog.log.error "VM START FAILED: vm=#{vm} try=#{try}/5"
						return {status: 500, data: "VM '#{vm}' start failed" }
					end
				end
			else
				MyLog.log.info "VM START SUCCESS: try=#{try}/5 vm=#{vm}"
				return {status: 200, data: "VM '#{vm}' started" } # exit if successful
			end
		end # eof loop
	end

	def self.stop_vm(vm, sync=true)
		MyLog.log.info "VM STOP CALLED: vm=#{vm}"
		retry_sleep = 2 # seconds to wait before retry
		(0..5).each do |try|
			MyLog.log.debug "VM STOP: try=#{try}/5 vm=#{vm}"
			result = VirtualBox.vboxmanage("controlvm #{esc(vm)} poweroff 2>&1", sync)
			if result[:exitstatus] != 0
				if result[:stdout].start_with? "VBoxManage: error: Could not find a registered machine named '#{vm}'" 
					MyLog.log.info "VM STOP SUCCESS: not registered try=#{try}/5 vm=#{vm}"
					return {status: 200, data: "VM '#{vm}' stopped" } # exit if successful
				elsif result[:stdout].start_with? "VBoxManage: error: Machine '#{vm}' is not currently running" 
					MyLog.log.info "VM STOP SUCCESS: already stopped try=#{try}/5 vm=#{vm}"
					return {status: 200, data: "VM '#{vm}' stopped" } # exit if successful
				else
					MyLog.log.warn "VM STOP: failed try=#{try}/5 vm=#{vm} \n#{result[:stdout]}"
					if try < 5
						sleep retry_sleep
						next  # go to next loop if not last
					else # last attempt failed
						MyLog.log.error "VM STOP FAILED: try=#{try}/5 vm=#{vm}"
						return {status: 500, data: "VM '#{vm}' stopping failed" }
					end
				end
			else # success status
				MyLog.log.info "VM STOP SUCCESS: try=#{try}/5 vm=#{vm}"
				return {status: 200, data: "VM '#{vm}' stopped" } # exit if successful
			end
		end # eof loop
	end

	def self.pause_vm(vm, sync=true)
 		MyLog.log.info "VM PAUSE CALLED: vm=#{vm}"
		retry_sleep = 1 # seconds to wait before retry
		(0..5).each do |try|
			MyLog.log.debug "VM PAUSE: try=#{try}/5 vm=#{vm}"
			result = VirtualBox.vboxmanage("controlvm #{esc(vm)} savestate 2>&1", sync)
			if result[:exitstatus] != 0
				if result[:stdout].start_with? "VBoxManage: error: Machine '#{vm}' is not currently running"
					MyLog.log.info "VM PAUSE SUCCESS: already paused try=#{try}/5 vm=#{vm}"
					return {status: 200, data: "VM '#{vm}' paused" } # exit if successful
				else
					MyLog.log.warn "VM PAUSE: failed try=#{try}/5 vm=#{vm} \n#{result[:stdout]}"
					if try < 5
						sleep retry_sleep
						next  # go to next loop if not last
					else # last attempt failed
						MyLog.log.error "VM PAUSE FAILED: try=#{try}/5 vm=#{vm}"
						return {status: 500, data: "VM '#{vm}' pause failed" }
					end
				end
			else # success status
				MyLog.log.info "VM PAUSE SUCCESS: try=#{try}/5 vm=#{vm}"
				return {status: 200, data: "VM '#{vm}' paused" } # exit if successful
			end
		end # eof loop
	end

	# NB: can not use resume to return from savestate, need to use start instead
	def self.resume_vm(vm, sync=true)
 		MyLog.log.info "VM RESUME CALLED: vm=#{vm}"
		retry_sleep = 1 # seconds to wait before retry
		(0..5).each do |try|
			MyLog.log.debug "VM RESUME: try=#{try}/5 vm=#{vm}"
			result = VirtualBox.vboxmanage("controlvm #{esc(vm)} resume 2>&1", sync)
			if result[:exitstatus] != 0
				MyLog.log.warn "VM RESUME: failed try=#{try}/5 vm=#{vm} \n#{result[:stdout]}"
				if try < 5
					sleep retry_sleep
					next  # go to next loop if not last
				else # last attempt failed
					MyLog.log.error "VM RESUME FAILED: try=#{try}/5 vm=#{vm}"
					return  {status: 500, data: "VM '#{vm}' resume failed" }
				end
			else # success status
				MyLog.log.info "VM RESUME SUCCESS: try=#{try}/5 vm=#{vm}"
				return {status: 200, data: "VM '#{vm}' resumed" }  # exit if successful
			end
		end # eof loop
	end

	def self.delete_vm(vm, sync=true)
		MyLog.log.info "VM DELETE CALLED: vm=#{vm}"
		retry_sleep = 2 # seconds to wait before retry
		(0..5).each do |try|
			MyLog.log.debug "VM DELETE: try=#{try}/5 vm=#{vm}"
			result = VirtualBox.vboxmanage("unregistervm #{esc(vm)} --delete 2>&1")
			if result[:exitstatus] != 0
				if result[:stdout].start_with? "VBoxManage: error: Could not find a registered machine named '#{vm}'"
					MyLog.log.info "VM DELETE SUCCESS: already deleted try=#{try}/5 vm=#{vm}"
					return {status: 200, data: "VM '#{vm}' already deleted" } # exit if successful
				else
					MyLog.log.warn "VM DELETE: failed try=#{try}/5 vm=#{vm} \n#{result[:stdout]}"
					if try < 5
						sleep retry_sleep
						next  # go to next loop if not last
					else # last attempt failed
						MyLog.log.error "VM DELETE FAILED: try=#{try}/5 vm=#{vm}"
						return {status: 500, data: "VM '#{vm}' delete failed" }
					end
				end
			else # success
				MyLog.log.info "VM DELETE SUCCESS: try=#{try}/5 vm=#{vm}"
				return {status: 200, data: "VM '#{vm}' deleted" }  # exit if successful
			end
		end
	end

	def self.cleanup_vm(vm)
		MyLog.log.debug "CLEANUP VM CALLED: vm=#{vm}"
		conf = VirtualBox.system_properties
		if conf[:status]==200
			path = "#{conf[:data]["Default machine folder"]}/#{vm}"
			begin
				files = Dir.entries(path)
				MyLog.log.debug "CLEANUP VM: deleting path=#{path}"
				FileUtils.rm_rf(path)
				MyLog.log.debug "CLEANUP VM SUCCESS: deleted path=#{path} vm=#{vm}"
				return {status: 200, data: "VM '#{vm}' files deleted"}
			rescue Errno::EACCES => e
				MyLog.log.warn e.message
				MyLog.log.warn "CLEANUP VM FAILED: permission denied path=#{path} vm=#{vm}"
				return {status: 500, data: "failed to delete VM '#{vm}' files"}
			rescue Errno::ENOENT => e 
				MyLog.log.debug e.message
				MyLog.log.debug "CLEANUP VM SUCCESS: already deleted (ENOENT) path=#{path} vm=#{vm}"
				return {status: 200, data: "VM '#{vm}' files already deleted"}
			rescue Exception => e
				MyLog.log.warn e
				MyLog.log.warn "CLEANUP VM FAILED: unknown error path=#{path} vm=#{vm}"
				return {status: 500, data: "failed to delete VM '#{vm}' files"}
			end
		else # failed at props
			MyLog.log.warn "CLEANUP VM FAILED: \n#{conf[:data]}"
			return {status: 500, data: "failed to delete VM '#{vm}' files"}
		end
	end

	def self.system_properties
		MyLog.log.debug "SYS PROPERTIES CALLED"
		result = VirtualBox.vboxmanage('list systemproperties', false)
		if result[:exitstatus] != 0
			MyLog.log.error "SYS PROPERTIES FAILED: \n#{result[:stdout]}"
			return {status: 500, data: "SERVER ERROR"}			
		end

		vm = {}
		if result 
			result[:stdout].split(/\n+/).each do |row|
				if row.strip != ''
					f = row.split(':')
					value = f.last.strip
					field = f.first.strip					
					vm[field] = value
				end
			end
		end # eof if result
		MyLog.log.debug "SYS PROPERTIES SUCCESS"
		return {status: 200, data: vm}
	end

	def self.clone_vm(vm, name, snapshot = '', sync=false) # TODO: does this take a lot of time, is it better not to sync?
		loginfo = "snapshot=#{snapshot} vmt=#{vm} vm=#{name}"
		MyLog.log.info "VM CLONE CALLED: #{loginfo}"
		key = Digest::SHA1.hexdigest(vm)
		template = MyCache.use_cache(key) do 
			VirtualBox.vm_info(vm, true, sync) # will try to find machine multiple times
		end		
		retry_sleep = 3 # seconds to wait before retry cloning
		if template[:status]==200 # if template is found
			# if snapshot is not defined look for latest
			if snapshot.blank? && !(snapshot === false) # do not get latest snapshot if specifically said to not use a snapshot
				snapshot = template[:data]['CurrentSnapshotName']
			end		
			loginfo = "snapshot=#{snapshot} vmt=#{vm} vm=#{name}"	
			if !snapshot.blank? # only if snapshot set
				(0..5).each do |try|
					MyLog.log.debug "VM CLONE: Cloning from snapshot try=#{try}/5 #{loginfo}"
					result = VirtualBox.vboxmanage("clonevm #{esc(vm)} --snapshot #{esc(snapshot)} --options link --name #{esc(name)} --register 2>&1", sync)
					if result[:exitstatus] != 0
						MyLog.log.warn "VM CLONE: Failed to clone vm try=#{try}/5 #{loginfo} \n#{result[:stdout]}"
						if try < 5
							sleep retry_sleep
							next  # go to next loop if not last. last will exit loop and clone from template directly
						end
					else # success!
						MyLog.log.info "VM CLONE SUCCESS: Cloned from snapshot try=#{try}/5 #{loginfo}"
						return {status: 200, data: "VM #{name} cloned from '#{vm}'" } # exit cloning if successful
					end
				end # eof loop
			end # eof if snapshot
			# we will reach here if cloning fails above or no snapshot is found, try to clone directly from machine instead
			loginfo = "vmt=#{vm} vm=#{name}"
			(0..5).each do |try|
				MyLog.log.debug "VM CLONE: Cloning from template try=#{try}/5 #{loginfo}"
				result = VirtualBox.vboxmanage("clonevm #{esc(vm)} --name #{esc(name)} --register 2>&1", sync)
				if result[:exitstatus] != 0
					MyLog.log.warn "VM CLONE: Failed to clone vm try=#{try}/5 #{loginfo} \n#{result[:stdout]}"
					if try < 5
						sleep retry_sleep
						next  # go to next loop if not last
					end
					# last loop
					MyLog.log.error "VM CLONE FAILED: Failed to clone vm try=#{try}/5 #{loginfo}"
					return {status: 500, data: "VM #{name} cloning from '#{vm}' failed" }
				else # success!
					MyLog.log.info "VM CLONE SUCCESS: Cloned from template try=#{try}/5 #{loginfo}"
					return {status: 200, data: "VM #{name} cloned from '#{vm}'" } # exit cloning if successful
				end
			end # eof loop
		else
			MyLog.log.error "VM CLONE FAILED: Failed to clone vm #{loginfo} \n#{template[:data]}"
			return template
		end
	end

	def self.set_groups(vm, groups, sync=false)
	 	loginfo = "vm=#{vm} groups=#{groups.join(',')}"
		MyLog.log.info "SET GROUPS CALLED: #{loginfo}"
		retry_sleep = 1
		(0..5).each do |try|
			MyLog.log.debug "SET GROUPS: try=#{try}/5 #{loginfo}"
			result = VirtualBox.vboxmanage("modifyvm #{esc(vm)} --groups #{esc(groups.join(','))} 2>&1", sync)
			if result[:exitstatus] != 0
				MyLog.log.warn "SET GROUPS: failed try=#{try}/5 #{loginfo}\n #{result[:stdout]}"
				if try < 5
					sleep retry_sleep
					next  # go to next loop if not last
				else # last attempt failed
					MyLog.log.error "SET GROUPS FAILED: try=#{try}/5 #{loginfo}"
					return {status: 500, data: "Failed to set VM '#{vm}' groups to #{groups.join(',')}" }
				end
			else # success
				MyLog.log.info "SET GROUPS SUCCESS: try=#{try}/5 #{loginfo}"
				return {status: 200, data: "VM '#{vm}' groups set to #{groups.join(',')}" }
			end
		end
	end

	def self.set_extra_data(vm, key, value = nil, sync=false)
	 	loginfo = "vm=#{vm} field=#{key} value=#{value}"
	 	MyLog.log.info "SET EXTRA DATA CALLED: #{loginfo}"
	 	retry_sleep = 1
		value = value == nil ? '' : esc(value)
		(0..5).each do |try|
			MyLog.log.debug "SET EXTRA DATA: try=#{try}/5 #{loginfo}"
			result = VirtualBox.vboxmanage("setextradata #{esc(vm)} #{esc(key)} #{value} 2>&1", sync)
			if result[:exitstatus] != 0
				MyLog.log.warn "SET EXTRA DATA: failed try=#{try}/5 #{loginfo}\n #{result[:stdout]}"
				if try < 5
					sleep retry_sleep
					next  # go to next loop if not last
				else # last attempt failed
					MyLog.log.error "SET EXTRA DATA FAILED: try=#{try}/5 #{loginfo}"
					return {status: 200, data: "Failed to set VM '#{vm}' extradata #{key} to #{value}" }
				end
			else # success
				MyLog.log.info "SET EXTRA DATA SUCCESS: try=#{try}/5 #{loginfo}"
				return {status: 200, data: "VM '#{vm}' extradata #{key} set to #{value}" }
			end
		end
	end

	def self.set_network(vm, slot, type, name='', sync=false)
		loginfo = "vm=#{vm} slot=#{slot} type=#{type} name=#{name}"
		MyLog.log.info "SET NETWORK CALLED: #{loginfo}"
		retry_sleep = 1
		cmd_prefix = "modifyvm #{esc(vm)}"
		name = esc(name)
		# compile a list of commands to run based on network type (1-2 commands)
		commands = []
		if type == 'nat'
			commands << "--nic#{slot} nat"
		elsif type == 'intnet'
			commands << "--nic#{slot} intnet"
			commands << "--intnet#{slot} #{name}"
		elsif type == 'bridgeadapter'
			commands << "--nic#{slot} bridged"
			commands << "--bridgeadapter#{slot} #{name}"
		elsif type == 'hostonlyadapter'
			commands << "--nic#{slot} hostonly"
			commands << "--hostonlyadapter#{slot} #{name}"
		end
		if commands.empty?
			return {status: 400, data: "Unsupported newtork type #{type} for VM '#{vm}'" }
		else
			# run each command
			commands.each do |cmnd|
				(0..5).each do |try|
					MyLog.log.debug "SET NETWORK: calling #{cmnd} try=#{try}/5 #{loginfo}"
					result = VirtualBox.vboxmanage("#{cmd_prefix} #{cmnd} 2>&1", sync)
					# try command again if failed
					if result[:exitstatus] != 0
						MyLog.log.warn "SET NETWORK: #{cmnd} failed try=#{try}/5 #{loginfo} \n#{result[:stdout]}"
						if try < 5
							sleep retry_sleep
							next  # go to next loop if not last
						else # last attempt failed
							MyLog.log.error "SET NETWORK FAILED: #{cmd_prefix} #{cmnd} try=#{try}/5 #{loginfo}"
							return {status: 500, data: "Failed to set VM '#{vm}' network slot #{slot} to #{type} #{name}" }
						end
					else # success 
						MyLog.log.info "SET NETWORK: #{cmnd} successful try=#{try}/5 #{loginfo}"
						break # break out of the loop, continue to the next command
					end
				end # eof loop
			end # eof commands
			return {status: 200, data: "VM '#{vm}' network slot #{slot} set to #{type} #{name}" } # we make it here if commands loop and the loop inside finish
		end
	end

	def self.set_running_network(vm, slot, type, name='', sync=false)
		loginfo = "vm=#{vm} slot=#{slot} type=#{type} name=#{name}"
		MyLog.log.info "SET RUNNING NETWORK CALLED: #{loginfo}"
		retry_sleep = 1
		cmd_prefix = "controlvm #{esc(vm)}"
		name = esc(name)
		command = ''
		# choose command based on type
		if type == 'null'
			command = "nic#{slot} null"
		elsif type == 'nat'
			command = "nic#{slot} nat"
		elsif type == 'intnet'
			command = "nic#{slot} intnet #{name}"
		elsif type == 'bridgeadapter'
			command = "nic#{slot} bridged #{name}"
		elsif type == 'hostonlyadapter'
			command = "nic#{slot} hostonly #{name}"
		end
		if command.blank?
			return {status: 400, data: "Unsupported newtork type #{type} for VM '#{vm}'" }
		else
			(0..5).each do |try|
				MyLog.log.debug "SET RUNNING NETWORK: try=#{try}/5 #{loginfo}"
				result = VirtualBox.vboxmanage("#{cmd_prefix} #{command} 2>&1", sync)
				# try command again if failed
				if result[:exitstatus] != 0
					MyLog.log.warn "SET RUNNING NETWORK: failed try=#{try}/5 #{loginfo} \n#{result[:stdout]}"
					if try < 5
						sleep retry_sleep
						next  # go to next loop if not last
					else # last attempt failed
						MyLog.log.error "SET RUNNING NETWORK FAILED: #{cmd_prefix} #{command} try=#{try}/5 #{loginfo}"
						return {status: 500, data: "Failed to set VM '#{vm}' network slot #{slot} to #{type} #{name}" }
					end
				else # success 
					MyLog.log.info "SET RUNNING NETWORK SUCCESS: try=#{try}/5 #{loginfo}"
					return {status: 200, data: "VM '#{vm}' network slot #{slot} set to #{type} #{name}" }
				end
			end # eof loop
		end
	end

	# adds a disk to a stopped vm
	def self.set_drive(vm, controller, port, device, type, path, sync=false)
		allowedtypes = ['hdd', 'dvddrive', 'fdd']
		return {status: 400, data:"Unsupported drive type #{type} for VM #{vm}, use: #{allowedtypes.join(', ')}"} unless allowedtypes.include?(type)
		# storageattach  #{vm} --storagectl #{controller} --port #{port} --device #{device} --type #{type} --medium #{path}
		loginfo = "vm=#{vm} controller=#{controller} port=#{port} device=#{device} type=#{type} path=#{path}"
		MyLog.log.info "SET DRIVE CALLED: #{loginfo}"
		#MyLog.log.debug "storageattach  #{esc(vm)} --storagectl #{esc(controller)} --port #{port.to_i} --device #{device.to_i} --type #{esc(type)} --medium #{esc(path)}"
		retry_sleep = 1
		hot = ( controller=="SATA" ? '--hotpluggable on' : '')
		(0..5).each do |try|
			MyLog.log.debug "SET DRIVE: try=#{try}/5 #{loginfo}"
			result = VirtualBox.vboxmanage("storageattach #{esc(vm)} --storagectl #{esc(controller)} --port #{port.to_i} --device #{device.to_i} --type #{esc(type)} --medium #{esc(path)} #{hot} 2>&1", sync)
			MyLog.log.info result
			if result[:exitstatus] != 0
				if result[:stdout].start_with? "VBoxManage: error: Could not find a controller named '#{controller}'"
					# invalid controller, relay existing controllers?
					return {status: 500, data: "VM '#{vm}' failed to set drive #{controller} #{port} #{device} to #{path} - controller not available"  }
				elsif result[:stdout].start_with? "VBoxManage: error: No storage device attached to device slot #{device} on port #{port} of controller '#{controller}'"
					# the port is not available. is the machine running?
					return {status: 500, data: "VM '#{vm}' failed to set drive #{controller} #{port} #{device} to #{path} - port not available"  }
				elsif result[:stdout].start_with? "VBoxManage: error: The machine is not mutable (state is Running)"
					return {status: 500, data: "VM '#{vm}' failed to set drive #{controller} #{port} #{device} to #{path} - machine is running"  }
				elsif result[:stdout].start_with? "VBoxManage: error: Could not mount the media/drive '#{path}' (VERR_PDM_MEDIA_LOCKED)"
					return {status: 500, data: "VM '#{vm}' failed to set drive #{controller} #{port} #{device} to #{path} - unable to overwrite existing drive"  }
				else
					MyLog.log.warn "SET DRIVE: failed try=#{try}/5 vm=#{vm} \n#{result[:stdout]}"
					if try < 5
						sleep retry_sleep
						next  # go to next loop if not last
					else # last attempt failed
						MyLog.log.error "SET DRIVE FAILED: try=#{try}/5 vm=#{vm}"
						return {status: 500, data: "VM '#{vm}' failed to set drive #{controller} #{port} #{device} to #{path}"  }
					end
				end
			else
				MyLog.log.info "SET DRIVE SUCCESS: try=#{try}/5 #{loginfo}"
				return {status: 200, data: "VM '#{vm}' drive #{controller} #{port} #{device} set to #{path}" }
			end
		end # eof retry
	end

	# removes a disk from a stopped vm
	def self.unset_drive(vm, controller, port, device, sync=false)
		loginfo = "vm=#{vm} controller=#{controller} port=#{port} device=#{device}"
		MyLog.log.info "UNSET DRIVE CALLED: #{loginfo}"
		MyLog.log.debug "storageattach  #{esc(vm)} --storagectl #{esc(controller)} --port #{port.to_i} --device #{device.to_i} --medium emptydrive"
		retry_sleep = 1
		(0..5).each do |try|
			MyLog.log.debug "UNSET DRIVE: try=#{try}/5 #{loginfo}"
			result = VirtualBox.vboxmanage("storageattach #{esc(vm)} --storagectl #{esc(controller)} --port #{port.to_i} --device #{device.to_i} --medium emptydrive --forceunmount 2>&1", sync)
			if result[:exitstatus] != 0
				if result[:stdout].start_with? "VBoxManage: error: No storage device attached to device slot #{device} on port #{port} of controller '#{controller}'"
					MyLog.log.info "UNSET DRIVE SUCCESS: try=#{try}/5 #{loginfo}"
					return {status: 200, data: "VM '#{vm}' drive #{controller} #{port} #{device} was already removed" }
				elsif result[:stdout].start_with? "VBoxManage: error: Controller '#{controller}' does not support hotplugging"
					# the drive can not be removed is the machine running?
					return {status: 500, data: "VM '#{vm}' failed to remove drive #{controller} #{port} #{device} - hotplugging disabled"  }
				else
					MyLog.log.warn "UNSET DRIVE: failed try=#{try}/5 vm=#{vm} \n#{result[:stdout]}"
					if try < 5
						sleep retry_sleep
						next  # go to next loop if not last
					else # last attempt failed
						MyLog.log.error "UNSET DRIVE FAILED: try=#{try}/5 vm=#{vm}"
						return {status: 500, data: "VM '#{vm}' failed to set drive #{controller} #{port} #{device} to 'emptydrive'"  }
					end
				end
			else
				MyLog.log.info "UNSET DRIVE SUCCESS: try=#{try}/5 #{loginfo}"
				return {status: 200, data: "VM '#{vm}' drive #{controller} #{port} #{device} set to 'emptydrive'" }
			end
		end # eof retry
	end

	def self.guestcontrol_run(vm, username='', password='', command={},sync=false)
		MyLog.log.info "GC RUN CALLED: vm=#{vm}"
		return {status:400, data:"Command not specified"} if command.blank?
		username = "--username #{esc(username)}" unless username.blank?
		password = "--password #{esc(password)}" unless password.blank?
		params = ''
		params = command[:params].map{|p| esc(p) }.join(' ') unless command[:params].blank?
	 	MyLog.log.info "vboxmanage guestcontrol #{esc(vm)} #{username} #{password} run --exe #{esc(command['exe'])} -- #{params} 2>&1"
	 	result = VirtualBox.vboxmanage("guestcontrol #{esc(vm)} #{username} #{password} run --exe #{esc(command['exe'])} -- #{params} 2>&1", sync)
	 	MyLog.log.info result[:stdout]
	 	MyLog.log.info "GC RUN END: vm=#{vm}"
	 	return {status: 200, data: result[:stdout], exit: result[:exitstatus]}
	end
	
	# TODO: should this be also with retries?
	def self.reset_rdp(vm, sync=false)
		MyLog.log.info "RESET RDP CALLED: vm=#{vm}"
	 	result = VirtualBox.vboxmanage("controlvm #{esc(vm)} vrde off 2>&1", sync)
		if result[:exitstatus] != 0
			if result[:stdout].start_with? "VBoxManage: error: Machine '#{vm}' is not currently running"
				MyLog.log.warn "RESET RDP FAILED: not running vm=#{vm}\n#{result[:stdout]}"
				return {status: 405, data: "Unable to disable RDP for VM '#{vm}' that is not currently running" }
			else
				MyLog.log.error "RESET RDP FAILED: disable vm=#{vm}\n#{result[:stdout]}"
				return {status: 500, data: "Unable to disable RDP for VM '#{vm}'" }
			end
		end
		MyLog.log.info "RESET RDP: RDP disable successful, enabling vm=#{vm}"

		result = VirtualBox.vboxmanage("controlvm #{esc(vm)} vrde on 2>&1", sync)
		if result[:exitstatus] != 0
			if result[:stdout].start_with? "VBoxManage: error: Machine '#{vm}' is not currently running"
				MyLog.log.warn "RESET RDP FAILED: not running vm=#{vm}\n#{result[:stdout]}"
				return {status: 405, data: "Unable to enable RDP for VM '#{vm}' that is not currently running" }
			else
				MyLog.log.error "RESET RDP FAILED: enable vm=#{vm}\n#{result[:stdout]}"
				return {status: 500, data: "Unable to enable RDP for VM '#{vm}'" }
			end
		end
		MyLog.log.info "RESET RDP SUCCESS: vm=#{vm}"
		return {status: 200, data: "RDP reset for VM '#{vm}'" }
	end

	# TODO: add retry?
	def self.take_snapshot(vm, sync=false)
		MyLog.log.info "TAKE SNAPSHOT CALLED: vm=#{vm}"
		key = Digest::SHA1.hexdigest(vm)
		info = MyCache.use_cache(key) do 
			VirtualBox.vm_info(vm, true, sync) # will try to find machine multiple times
		end
		if info[:status]==200
			if info[:data]['VMState']=="poweroff"
				vmname = vm.gsub("-template",'')
				nr = info[:data]['CurrentSnapshotName'] ? info[:data]['CurrentSnapshotName'].gsub("#{vmname}-",'').gsub('-template','').to_i + 1 : 1
				name = "#{vmname}-#{nr}-template"
				result = VirtualBox.vboxmanage("snapshot #{esc(vm)} take #{esc(name)} --description "+'"'+"#{Time.now}"+'" 2>&1', sync)
				if result[:exitstatus] != 0
					MyLog.log.error "TAKE SNAPSHOT FAILED: vm=#{vm}\n#{result[:stdout]}"
					return {status: 500, data: "Unable to take snapshot of '#{vm}'"}
				end
				MyLog.log.info "TAKE SNAPSHOT SUCCESS: vm=#{vm}"
				return {status: 200, data: "Took snapshot #{name} of '#{vm}'"}
			else
				return {status: 405, data: "Unable to take snapshot of '#{vm}' when not powered off"}
			end
		else
			MyLog.log.error "VM CLONE FAILED: Failed to clone vm #{loginfo} \n#{template[:data]}"
			return template
		end
	end


end