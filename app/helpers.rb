
class MyLog
	def self.log
		if @logger.nil?
			@logger = Logger.new STDOUT
			@logger.level = Logger::DEBUG # will be overwritten by a before filter based on db value
			@logger.datetime_format = '%Y-%m-%d %H:%M:%S '
		end
		@logger
	end
end

class MyConf
	def self.root
		File.expand_path(File.dirname(__FILE__))
	end
	def self.load_conf
		if @config.nil?
			begin
				file = File.read("#{MyConf.root}/config.json");
				@config = JSON.parse(file)
			rescue Exception => e
				MyLog.log.error e.message
				exit 1 
			end			
		end
		@config
	end
	def self.token
		MyConf.load_conf
		if ENV['VBOXMANAGER_TOKEN']
			return ENV['VBOXMANAGER_TOKEN']
		elsif !@config['token'].blank?
			return @config['token']
		else
			false
		end
	end
	def self.mc_host
		if ENV['MEMCACHE_HOST']
			ENV['MEMCACHE_HOST']
		else
			"127.0.0.1:11211"
		end
	end
end

class MyCache
	def self.mem
		unless @mem
			@mem = Memcached.new(Sinatra::Application.settings.memcacher_server)
		end
		@mem
	end

	def self.use_cache(key, &block)
		return block.call unless Sinatra::Application.settings.memcacher_enabled
		begin
			MyLog.log.debug "MEMCACHE: getting value #{key}"
			output = MyCache.mem.get(key)
			MyLog.log.debug "MEMCACHE: got value #{key}"
		rescue Memcached::NotFound 
			MyLog.log.debug "MEMCACHE: getting value failed"
			output = block.call
			MyLog.log.debug "MEMCACHE: setting value #{key} to #{output}"
			MyCache.mem.set(key, output, Sinatra::Application.settings.memcacher_expiry)
		rescue Memcached::ServerIsMarkedDead => e
			MyLog.log.error e
			raise "Unable to connect to Cache"
		end
		output
	end
	
	def self.set_cache(key, &block)
		return block.call unless Sinatra::Application.settings.memcacher_enabled  
		begin
			output = block.call
			MyLog.log.debug "MEMCACHE: setting value #{key} to #{output}"
			MyCache.mem.set(key, output, Sinatra::Application.settings.memcacher_expiry)
		rescue Memcached::NotFound

		rescue Memcached::ServerIsMarkedDead => e
			MyLog.log.error e
			raise "MEMCACHE: Unable to connect to Cache"
		end
		output
	end

	def self.expire(key)
		begin
			MyLog.log.debug "MEMCACHE: deleting value for #{key}"
			MyCache.mem.delete(key)
			MyLog.log.debug "MEMCACHE: deleted value for #{key}"
			true
		rescue Memcached::NotFound
			MyLog.log.debug "MEMCACHE: value for #{key} not found"
			false
		rescue Memcached::ServerIsMarkedDead => e
			MyLog.log.error e
			raise "MEMCACHE: Unable to connect to Cache"
		end
	end
end


def throw_error(code, message, log)
	MyLog.log.error log
	halt code, message
end

def to_json_array(result)
	rows = result.split(/\n+/)
	vms = []
	rows.each do |row|
		info = row.gsub('"','').gsub('{','').gsub('}','').split()
		vms << {name: info[0], uuid: info[1]}
	end
	vms
end

def esc(var)
	Shellwords.escape(var)
end

class String
	def blank?
		self.nil? || self.empty?
	end
end

class NilClass
	def blank?
		true
	end
end

class Array
	def blank?
		self.nil? || self.empty?
	end
end

class Hash
	def blank?
		self.nil? || self.keys.empty?
	end
end