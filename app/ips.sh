#!/bin/bash

TMPFILE=/tmp/runningvms$RANDOM

if [ -z "$1"  ]; then
	vboxmanage list runningvms | cut -d\" -f2 > $TMPFILE
else
	vboxmanage list runningvms | grep -i "$1" | cut -d\" -f2 > $TMPFILE
fi

while read p; do

echo ""
echo "VMName:		 $p"
vboxmanage guestproperty enumerate $p | grep IP 

done <"$TMPFILE"

rm "$TMPFILE"
