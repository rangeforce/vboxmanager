#!/usr/bin/env ruby
require 'sinatra'
require 'sinatra/reloader' if development?
require 'sinatra/respond_with'
require "sinatra/multi_route"
require "json"
require 'json/ext' # required for .to_json
require 'shellwords' # required to execute scripts safely
require 'logger'

require 'memcached'
require "base64"

require_relative 'virtualbox' # vboxmanage class
require_relative 'helpers' # useful methods

puts "STARTUP!!!  VBOXMANAGER_TOKEN=#{ENV['VBOXMANAGER_TOKEN']} MEMCACHE_HOST=#{ENV['MEMCACHE_HOST']} MEMCACHE_USERNAME=#{ENV['MEMCACHE_USERNAME']} MEMCACHE_PASSWORD=#{ENV['MEMCACHE_PASSWORD']} token=#{MyConf.token} mc_host=#{MyConf.mc_host}"

set :strict_paths, false

set :memcacher_client, nil
set :memcacher_enabled, true
set :memcacher_server, MyConf.mc_host
set :memcacher_expiry, 30 # seconds to hold vminfo?

before /.*/ do
	if request.url.match(/.json$/)
		request.accept.unshift('application/json')
		request.path_info = request.path_info.gsub(/.json$/,'')
	end

	begin
		body = request.body.read
		if !body.strip.empty? && JSON.parse(body)
			JSON.parse(body).each do |k,v|
				params[k.to_sym]=v
			end
		end
		apikey = params.delete(:apiKey)
		safe_params = params.deep_dup
		safe_params[:password] = "-snip-" if params[:password]
		MyLog.log.info "STARTING #{request.request_method} #{request.path_info} #{safe_params}"
		# token checking
		unless ['all', 'templates', 'stopped', 'running', nil, 'search', 'ips'].include? request.path_info.split('/')[2]
			throw_error(500, "Server error", "API token not set in configuration") unless MyConf.token
			throw_error(401, "API key not supplied", "Request without token") if apikey.blank?
			throw_error(401, "Invalid API key", "Request with unknown token") if apikey != MyConf.token
		end

	rescue JSON::ParserError => e
		safe_params = params.deep_dup
		safe_params[:password] = "-snip-" if params[:password]
		MyLog.log.debug safe_params
		MyLog.log.warn "ignoring invalid json"
	end
end

# update vm info?
after /.*/ do
	type = request.path_info.split('/')[2]
	unless ['all', 'templates', 'stopped', 'running', nil, 'search', 'ips', 'properties'].include?(type) # not public
		if response.status == 200
			if ['info'].include?(type)
				# do nothing, because the route itself updates the cache
			elsif ['delete'].include?(type)
				# remove chache entry? or let it expire?
				unless params[:name].blank?
					key = Digest::SHA1.hexdigest params[:name]
					result = MyCache.expire(key)
				end
			else
				# update vm info if name given
				unless params[:name].blank?
					key = Digest::SHA1.hexdigest params[:name]
					result = MyCache.set_cache(key) do 
						VirtualBox.vm_info(params[:name], false, false) # no need to retry nor sync here?
					end
				end
			end
		end # HTTP OK
	end # not public
end


get '/vms/running' do
	result = VirtualBox.running_vms
	response.status = result[:status] if result[:status]
	respond_to do |format|
		format.html { result[:data] }
		format.json { 
			if response.status==200
				to_json_array(result[:data]).to_json
			else
				result.to_json 
			end
		}
	end
end

get '/vms/stopped' do
	respond_to do |format|
		all = VirtualBox.vms # all vms
		response.status = all[:status] if all[:status]
		if response.status == 200
			all_vms = to_json_array(all[:data])
			running = VirtualBox.running_vms
			response.status = running[:status] if running[:status]
			if response.status == 200
				running_vms = to_json_array(running[:data])
				stopped = (all_vms-running_vms)
			
				format.html { stopped.map{|f| '"'+f[:name]+'" {'+f[:uuid]+'}'}.join("\n") }
				format.json { stopped.to_json }

			else # retunr error from getting running vms
				format.html { running[:data] }
				format.json { running.to_json }
			end
		else # return errors from getting all machines
			format.html { all[:data] }
			format.json { all.to_json }
		end
	end
end

get '/vms/all' do
	result = VirtualBox.vms
	response.status = result[:status] if result[:status]
	respond_to do |format|
		format.html { result[:data] }
		format.json { 
			if response.status==200
				to_json_array(result[:data]).to_json
			else
				result.to_json 
			end
		}
	end
end

get '/vms/templates' do
	result = VirtualBox.vms
	response.status = result[:status] if result[:status]
	respond_to do |format|
		if response.status == 200
			vms = to_json_array(result[:data])
			# filter templates
			filtered = vms.select{|v| v[:name].end_with?('template') }
			format.html { filtered.map{|f| '"'+f[:name]+'" {'+f[:uuid]+'}'}.join("\n") }
			format.json { filtered.to_json }
		else
			format.html { result[:data] }
			format.json { result.to_json }
		end
	end
end

get '/vms/search', '/vms/search/:search' do
	result = VirtualBox.vms
	response.status = result[:status] if result[:status]
	respond_to do |format|
		if response.status == 200
			vms = to_json_array(result[:data])
			# filter
			filtered = vms.select{|v| v[:name].include?(params[:search]) }
			format.html { filtered.map{|f| '"'+f[:name]+'" {'+f[:uuid]+'}'}.join("\n") }
			format.json { filtered.to_json }
		else
			format.html { result[:data] }
			format.json { result.to_json }
		end
	end
end

get '/vms/ips', '/vms/ips/:search' do
	content_type :html
	unless params[:search].blank?
		result = %x( ./ips.sh #{Shellwords.escape(params[:search])} )
		out = $?
		unless out == 0 #check if the child process exited cleanly.
			throw_error(500, "Unable to find vms", "got error #{result}")
		end
		result
	else # no searchword
		result = %x( ./ips.sh  )
		out = $?
		unless out == 0 #check if the child process exited cleanly.
			throw_error(500, "Unable to find vms", "got error #{result}")
		end
		result
	end
end

### token protected

# respond with version info
get '/status' do
	respond_to do |format|
		result = VirtualBox.versions
		response.status = result[:status] if result[:status]
		format.html { result[:data] }
		format.json { result.to_json }
	end
end

# check resources
get '/resources' do
	respond_to do |format|
		result = VirtualBox.resources
		response.status = result[:status] if result[:status]
		format.html { result[:data] }
		format.json { result.to_json }
	end
end

get '/vms/info', '/vms/info/:name' do
	respond_to do |format|
		if params[:name].blank?
			if params[:names].blank? || !params[:names].is_a?(Array)
				response.status = 400
				message = "name(s) not specified"
				format.html { message }
				format.json { {data: message, status: response.status}.to_json }
			else # given a array of names 
				results = []
				params[:names].each do |v|
					key = Digest::SHA1.hexdigest(v)
					results << MyCache.use_cache(key) do 
						VirtualBox.vm_info(v, params[:retry], params[:sync])
					end
				end
				format.html { results.to_json }
				format.json { results.to_json }
			end
		else
			key = Digest::SHA1.hexdigest(params[:name])
			result = MyCache.use_cache(key) do 
				VirtualBox.vm_info(params[:name], params[:retry], params[:sync])
			end
			response.status = result[:status] if result[:status]
			format.html { result[:data].to_json }
			format.json { result.to_json }
		end
	end
end

put '/vms/port' do
	respond_to do |format|
		if params[:name].blank?
			response.status = 400
			message = "name not specified"
			format.html { message }
			format.json { {data: message, status: response.status}.to_json }
		else
			result = VirtualBox.modify_vrdeport(params[:name], params[:range], params[:sync])
			response.status = result[:status] if result[:status]
			format.html { result[:data] }
			format.json { result.to_json }
		end
	end
end

put '/vms/start' do
	respond_to do |format|
		if params[:name].blank?
			response.status = 400
			message = "name not specified"
			format.html { message }
			format.json { {data: message, status: response.status}.to_json }
		else
			result = VirtualBox.start_vm(params[:name], params[:sync])
			response.status = result[:status] if result[:status]
			format.html { result[:data] }
			format.json { result.to_json }
		end
	end
end

put '/vms/stop' do
	respond_to do |format|
		if params[:name].blank?
			response.status = 400
			message = "name not specified"
			format.html { message }
			format.json { {data: message, status: response.status}.to_json }
		else
			result = VirtualBox.stop_vm(params[:name], params[:sync])
			response.status = result[:status] if result[:status]
			format.html { result[:data] }
			format.json { result.to_json }
		end
	end
end

put '/vms/pause' do
	respond_to do |format|
		if params[:name].blank?
			response.status = 400
			message = "name not specified"
			format.html { message }
			format.json { {data: message, status: response.status}.to_json }
		else
			result = VirtualBox.pause_vm(params[:name], params[:sync])
			response.status = result[:status] if result[:status]
			format.html { result[:data] }
			format.json { result.to_json }
		end
	end
end

put '/vms/resume' do
	respond_to do |format|
		if params[:name].blank?
			response.status = 400
			message = "name not specified"
			format.html { message }
			format.json { {data: message, status: response.status}.to_json }
		else
			result = VirtualBox.start_vm(params[:name], params[:sync]) # to return from savestate one needs to use start instead of resume
			response.status = result[:status] if result[:status]
			format.html { result[:data] }
			format.json { result.to_json }
		end
	end
end

delete '/vms/delete', '/vms/delete/:name' do
	respond_to do |format|
		if params[:name].blank?
			response.status = 400
			message = "name not specified"
			format.html { message }
			format.json { {data: message, status: response.status}.to_json }
		else
			result = VirtualBox.delete_vm(params[:name], params[:sync])
			VirtualBox.cleanup_vm(params[:name])
			response.status = result[:status] if result[:status]
			format.html { result[:data] }
			format.json { result.to_json }
		end
	end
end

post '/vms/clone' do
	respond_to do |format|
		if params[:name].blank? || params[:template].blank?
			response.status = 400
			message = "template or target machine name not specified"
			format.html { message }
			format.json { {data: message, status: response.status}.to_json }
		else
			result = VirtualBox.clone_vm(params[:template], params[:name], params[:snapshot], params[:sync])
			response.status = result[:status] if result[:status]
			format.html { result[:data] }
			format.json { result.to_json }
		end
	end
end

post '/vms/snapshot' do
	respond_to do |format|
		if params[:name].blank?
			response.status = 400
			message = "template name not specified"
			format.html { message }
			format.json { {data: message, status: response.status}.to_json }
		else
			result = VirtualBox.take_snapshot(params[:name], params[:sync])
			response.status = result[:status] if result[:status]
			format.html { result[:data] }
			format.json { result.to_json }
		end
	end
end

put '/vms/extradata' do
	respond_to do |format|
		if params[:name].blank?
			response.status = 400
			message = "name not specified"
			format.html { message }
			format.json { {data: message, status: response.status}.to_json }
		else
			result = {}
			if params[:key].blank? 
				if params[:values].blank? || !params[:values].is_a?(Array)
					# no key-value pair or extradata batch
					result = {status: 400, data:"no key-value pair or extradata array sent"}
				else
					# extradata sent as a batch
					results = []
					params[:values].each do |v|
						results << VirtualBox.set_extra_data(params[:name], v[:key], v[:value], params[:sync])
					end
					MyLog.log.debug results
					# return the highest status, and all the individual statuses
					statuses = results.map{|v| v[:status]}.flatten.uniq
					result = {status: statuses.max, data: results}
				end
			else
				result = VirtualBox.set_extra_data(params[:name], params[:key], params[:value], params[:sync])
			end
			response.status = result[:status] if result[:status]
			format.html { result[:data] }
			format.json { result.to_json }
		end
	end
end

put '/vms/groups' do
	respond_to do |format|
		if params[:name].blank?
			response.status = 400
			message = "name not specified"
			format.html { message }
			format.json { {data: message, status: response.status}.to_json }
		else
			result = VirtualBox.set_groups(params[:name], params[:groups], params[:sync])
			response.status = result[:status] if result[:status]
			format.html { result[:data] }
			format.json { result.to_json }
		end
	end
end

put '/vms/reset_rdp' do
	respond_to do |format|
		if params[:name].blank?
			response.status = 400
			message = "name not specified"
			format.html { message }
			format.json { {data: message, status: response.status}.to_json }
		else
			result = VirtualBox.reset_rdp(params[:name], params[:sync])
			response.status = result[:status] if result[:status]
			format.html { result[:data] }
			format.json { result.to_json }
		end
	end
end

put '/vms/set_network' do
	respond_to do |format|
		if params[:name].blank?
			response.status = 400
			message = "name not specified"
			format.html { message }
			format.json { {data: message, status: response.status}.to_json }
		else
			result = VirtualBox.set_network(params[:name], params[:slot], params[:type], params[:nw_name], params[:sync])
			response.status = result[:status] if result[:status]
			format.html { result[:data] }
			format.json { result.to_json }
		end
	end
end

put '/vms/set_running_network' do
	respond_to do |format|
		if params[:name].blank?
			response.status = 400
			message = "name not specified"
			format.html { message }
			format.json { {data: message, status: response.status}.to_json }
		else
			result = VirtualBox.set_running_network(params[:name], params[:slot], params[:type], params[:nw_name], params[:sync])
			response.status = result[:status] if result[:status]
			format.html { result[:data] }
			format.json { result.to_json }
		end
	end
end

put '/vms/drive' do
	respond_to do |format|
		if params[:name].blank?
			response.status = 400
			message = "name not specified"
			format.html { message }
			format.json { {data: message, status: response.status}.to_json }
		else
			if params[:drives] && params[:drives].is_a?(Array)
				results = []
				params[:drives].each do |v|
					results <<  VirtualBox.set_drive(params[:name], v[:controller], v[:port], v[:device], v[:type], v[:path], params[:sync])
				end
				MyLog.log.debug results
				# return the highest status, and all the individual statuses
				statuses = results.map{|v| v[:status]}.flatten.uniq
				result = {status: statuses.max, data: results}
			else
				result = VirtualBox.set_drive(params[:name], params[:controller], params[:port], params[:device], params[:type], params[:path], params[:sync])
			end
			response.status = result[:status] if result[:status]
			format.html { result[:data] }
			format.json { result.to_json }
		end
	end
end
delete '/vms/drive' do
	respond_to do |format|
		if params[:name].blank?
			response.status = 400
			message = "name not specified"
			format.html { message }
			format.json { {data: message, status: response.status}.to_json }
		else
			if params[:drives] && params[:drives].is_a?(Array)
				results = []
				params[:drives].each do |v|
					results <<  VirtualBox.unset_drive(params[:name], v[:controller], v[:port], v[:device], params[:sync])
				end
				MyLog.log.debug results
				# return the highest status, and all the individual statuses
				statuses = results.map{|v| v[:status]}.flatten.uniq
				result = {status: statuses.max, data: results}
			else
				result = VirtualBox.unset_drive(params[:name], params[:controller], params[:port], params[:device], params[:sync])
			end
			response.status = result[:status] if result[:status]
			format.html { result[:data] }
			format.json { result.to_json }
		end
	end
end

# guestcontrol run
put '/vms/gc/run' do
	respond_to do |format|
		if params[:name].blank?
			response.status = 400
			message = "name not specified"
			format.html { message }
			format.json { {data: message, status: response.status}.to_json }
		else
			result = VirtualBox.guestcontrol_run(params[:name], params[:username], params[:password], params[:cmd], params[:sync])
			response.status = result[:status] if result[:status]
			format.html { result[:data] }
			format.json { result.to_json }
		end
	end
end

get '/system/properties' do 
	respond_to do |format|
		result = VirtualBox.system_properties
		response.status = result[:status] if result[:status]
		format.html { result[:data].to_json }
		format.json { result.to_json }
	end
end

get '/' do
	content_type :json
	{ endpoints: {
		GET: (Sinatra::Application.routes["GET"]||[]).map{|m| m[0].to_s }-['*'] ,
		POST: (Sinatra::Application.routes["POST"]||[]).map{|m| m[0].to_s }-['*'] ,
		PUT: (Sinatra::Application.routes["PUT"]||[]).map{|m| m[0].to_s }-['*'] ,
		DELETE: (Sinatra::Application.routes["DELETE"]||[]).map{|m| m[0].to_s }-['*'] 
		}
	 }.to_json.to_s
end

# catcher route
route :get, :post, :put, :delete, '*' do
	response.status = 404
	content_type :html
	'route not found'
end

=begin
result = %x(ruby write_results_to_file.rb #{Shellwords.escape(labuser.to_json)} )
out = $?
unless out == 0 #check if the child process exited cleanly.
	logger.error "got error #{result}"
end 
=end
