#!/bin/bash


apt update
apt install memcached libmemcached-tools pwgen -y

#disable UDP
echo '-U 0' >> /etc/memcached.conf
# enable SASL
echo '-S' >> /etc/memcached.conf

systemctl restart memcached

apt install sasl2-bin libsasl2-dev -y

mkdir -p /etc/sasl2

cat > /etc/sasl2/memcached.conf <<END
mech_list: plain
log_level: 5
sasldb_path: /etc/sasl2/memcached-sasldb2
END
VBOX_TOKEN=$(pwgen 20 1)
PASS=$(pwgen 20 1)
echo "memcached SASL username:mc password: ${PASS}" >> /root/i-tee-passwords.txt
echo $PASS | saslpasswd2 -a memcached -c -p -f /etc/sasl2/memcached-sasldb2 mc
chown memcache:memcache /etc/sasl2/memcached-sasldb2

echo "MEMCACHE_USERNAME=mc" >> /etc/i-tee/memcache.env
echo "MEMCACHE_PASSWORD=${PASS}" >> /etc/i-tee/memcache.env
echo "VBOXMANAGER_TOKEN=${VBOX_TOKEN}" >> /etc/i-tee/memcache.env
sed -i -e "s|REPLACE_WITH_MEMCACHE_TOKEN|$VBOX_TOKEN|g" /etc/i-tee/config.yaml

chmod go= /etc/i-tee/memcache.env

apt install ruby ruby-dev build-essential -y
gem install bundler
cd app
bundle install
cd ..
test -d /usr/local/lib/systemd/system/ || mkdir -p /usr/local/lib/systemd/system/
cp vboxManager.service /usr/local/lib/systemd/system/
systemctl daemon-reload 
systemctl enable vboxManager.service
systemctl start vboxManager.service


echo "start lab-proxy VM and do: scp ips.sh hop@lab-proxy:"


